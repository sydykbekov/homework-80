const express = require('express');
const router = express.Router();

const createRouter = (db) => {
    // Product index
    router.get('/', (req, res) => {
        db.query('SELECT * FROM `Предмет учета`', function (error, results) {
            if (error) throw error;

            res.send(results);
        });
        // res.send(db.getData());
    });

    // Product create
    router.post('/', (req, res) => {
        const product = req.body;

        db.addItem(product).then(result => {
            res.send(result);
        });
    });

    // Product get by ID
    router.get('/:id', (req, res) => {
        db.query('SELECT * FROM `Предмет учета` WHERE Идентификатор=' + req.params.id + ';', function (error, results) {
            if (error) throw error;

            res.send(results);
        });
    });

    router.delete('/:id', (req, res) => {
        db.query('DELETE * FROM `Предмет учета` WHERE Идентификатор=' + req.params.id + ';', function (error, results) {
            if (error) throw error;

            res.send(results);
        });
    });

    return router;
};

module.exports = createRouter;