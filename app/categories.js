const express = require('express');
const router = express.Router();

const createRouter = (db) => {
    // Product index
    router.get('/', (req, res) => {
        db.query('SELECT * FROM `Категория предмета`', function (error, results) {
            if (error) throw error;

            res.send(results);
        });
    });

    // Product create
    router.post('/', (req, res) => {
        const product = req.body;

        db.addItem(product).then(result => {
            res.send(result);
        });
    });

    router.put('/:id', (req, res) => {
        const category = req.body;

        let word = '';

        for(let key in category) {

            word += key + ' =' + '"' + category[key] + '" ';
        }

        console.log(word);

        db.query('UPDATE `Категория предмета` SET ' + word + 'WHERE id=' + req.params.id + ';', function (error, results) {
            if (error) throw error;

            res.send(results);
        });
    });

    // Product get by ID
    router.delete('/:id', (req, res) => {
        db.query('DELETE FROM `Категория предмета` WHERE id=' + req.params.id + ';', function (error, results) {
            if (error) throw error;

            res.send(results);
        });
    });


    return router;
};

module.exports = createRouter;