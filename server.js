const express = require('express');
const fileDb = require('./fileDb');
const items = require('./app/items');
const app = express();
const cors = require('cors');
const mysql      = require('mysql');
const categories = require('./app/categories');
const places = require('./app/places');

const port = 8000;

app.use(express.json());
app.use(cors());

const connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'root',
    database : 'office'
});

connection.connect((err) => {
    if (err) throw err;

    app.use('/items', items(connection));

    app.use('/categories', categories(connection));

    app.use('/places', places(connection));

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
});
